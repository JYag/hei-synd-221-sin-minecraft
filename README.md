**HES-SO Valais/Wallis - 2020-2021 Project Electrical Age**

**Group 6** - Léo Agrusi, Jérémie Gay

_**This is the project of the 4th semester of TCP. It includes a controller that manage :**_

- The communication between a Minecraft World and a java program. 
- The commmand with a web page (can be downloaded here) :

     https://cyberlearn.hes-so.ch/pluginfile.php/3539553/mod_label/intro/WebClient.zip.
- Data report with Grafana (Grafana configuration can be downloaded here) :

     https://cyberlearn.hes-so.ch/pluginfile.php/3509054/mod_label/intro/DashboardGrafana.json.
- Grid regulator in a Minecraft World with specifics mods (solar, wind and coal producer and factory, house and bunker consumers).


 **With the grid regulator,the best score we reach is 449’914 on 2.5 days (see folder simulation).**
 
 - To do that we seperated the day in 4 parts. Night, start of the day, day, and start of the night. For each case we act differently to increase the final score.

___________________________________________________________________________________________________________________


**Code execution :**

_**To lunch the controller you have to run the Minecraft.jar file in the folder "out" with those arguments :**_

     "<db_host> <db_name> <modbus_home> <modbus_port> [-modbus4j]"

 **You MUST also have the META-INF folder in the same directory as the Minecraft.jar before running it !!!**

 _Example of run command:_
 
     java -jar Minecraft.jar influx.sdi.hevs.ch SIn16 localhost 1502 -modbus4j

