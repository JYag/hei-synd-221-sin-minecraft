package ch.hevs.isi;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Timer;
import java.util.TimerTask;

public class SmartController {
    float factorySetPoint = 0.5f;
    float coalSetPoint = 0f;
    float gridRefVoltage = 800;

    /**
     * Regulation of the production of energy and the consumption of the factory.
     * The purpose is to obtain the best score with some conditions
     */
    public SmartController() {
        //Coal
        FloatDataPoint coal = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("REMOTE_COAL_SP");
        FloatDataPoint stockCoal = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("COAL_AMOUNT");
        //Factory
        FloatDataPoint factory = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("REMOTE_FACTORY_SP");

        //Battery
        FloatDataPoint batteryPercent = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("BATT_CHRG_FLOAT");
        FloatDataPoint batteryGive = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("BATT_P_FLOAT");

        //Time
        FloatDataPoint time = (FloatDataPoint) DataPoint.getFloatDataPointFromLabel("CLOCK_FLOAT");

        // Update every 2 seconds
        Timer timer = new Timer();
        long period = 2000;
        timer.scheduleAtFixedRate(new TimerTask() {
            int step = 0;

            @Override
            public void run() {
                switch (step) {
                    case 0: // Time detection
                        if (time.getValue() > 0.83f || time.getValue() <= 0.20f) { // Night
                            step = 10;
                        } else if (time.getValue() > 0.20f && time.getValue() <= 0.30f) { // Start of the day
                            step = 20;
                        } else if (time.getValue() >= 0.30f && time.getValue() <= 0.70f) { // Day
                            step = 30;
                        } else if (time.getValue() > 0.65f && time.getValue() <= 0.83f) { // Start of the night
                            step = 40;
                        }
                        break;

                    case 10: // Night - Start coal and stop factory
                        factory.setValue(0f);
                        if (batteryPercent.getValue() < 0.40f) {
                            coal.setValue(1f);
                        } else {
                            coal.setValue(0.6f);
                        }
                        step = 0;
                        break;

                    case 20: // Start of the day
                        if (stockCoal.getValue() < 0.3) {
                            coal.setValue(1f);
                        }
                        if (batteryPercent.getValue() < 0.15f) {
                            coal.setValue(coal.getValue() - 0.1f);
                        }
                        if (batteryPercent.getValue() > 0.50f && factory.getValue() < 0.50f) {
                            factory.setValue(factory.getValue() + 0.1f);
                        }
                        step = 0;
                        break;
                    case 30: // Day - charge battery and factory production
                        coal.setValue(0f);
                        if (batteryPercent.getValue() > 0.55f) {
                            factory.setValue(batteryPercent.getValue() + 0.15f);
                        } else {
                            factory.setValue(0f);
                        }
                        if (batteryGive.getValue() < 0f) {
                            factory.setValue(factory.getValue() - 0.1f);
                        }
                        step = 0;
                        break;
                    case 40: // Start of the night
                        coal.setValue(0.6f);
                        if (batteryPercent.getValue() < 0.80f) {
                            factory.setValue(0f);
                        } else {
                            factory.setValue(factory.getValue() - 0.1f);
                        }
                        step = 0;
                        break;
                }
            }
        }, 0, period);
    }
}