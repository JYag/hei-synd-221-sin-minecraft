package ch.hevs.isi.database;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;

/**
 * Database Connector
 */
public class DatabaseConnector implements DataPointListener {
    private static DatabaseConnector theDbc = null;

    static String URL;
    static String mdp;
    static String encoding;
    static String type = "binary/octet-stream";

    /**
     * DatabaseConnector config
     *
     * @param url url of the site
     * @param db username
     * @param user username
     * @param pass password
     */
    public static void config(String url, String db, String user, String pass) {
        URL = "https://" + url +"/write?db="+db;
        mdp = user+":"+pass;
        encoding = Base64.getEncoder().encodeToString(mdp.getBytes());
    }

    /**
     * Database Connector
     */
    private DatabaseConnector() {
    }

    /**
     * The static method getInstance() instance a Database connector
     * if no DatabaseConnector have already been created
     *
     * @return DatabaseConnector
     */
    public static DatabaseConnector getInstance() {
        if (theDbc == null) {
            theDbc = new DatabaseConnector();
        }
        return theDbc;
    }

    /**
     * Push a new value with the given label to the DataBase
     *
     * @param label the name of the DataPoint
     * @param value the value of the DataPoint
     */
    private void pushToDataBaseConnector(String label, String value) {
        try {
            URL url1 = new URL(URL);
            HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Content-Type", type);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            //System.out.println(label + " value=" + value);


            writer.write(label + " value=" + value);
            writer.flush();
            int responseCode = connection.getResponseCode();
            //System.out.println(responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            if (responseCode == 204) {
                while ((in.readLine()) != null) {
                }
            }
            connection.disconnect();
        } catch (ProtocolException e) {
            e.printStackTrace();
            System.out.println("DatabaseConnector -> " + "pushToDataBaseConnector() -> " + "Protocol Exception" + e.getMessage());
        } catch (MalformedURLException e) {
            System.out.println("DatabaseConnector -> " + "pushToDataBaseConnector() -> " + "Malformed Exception" + e.getMessage());
        } catch (IOException e) {
            System.out.println("DatabaseConnector -> " + "pushToDataBaseConnector() -> " + "IO Exception" + e.getMessage());
        }
    }

    /**
     * Push the new value of a float Datapoint to database
     *
     * @param fdp Float data point
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        pushToDataBaseConnector(fdp.getLabel(), String.valueOf(fdp.getValue()));
    }

    /**
     * Push the new value of a binary Datapoint to database
     *
     * @param bdp Binary data point
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        pushToDataBaseConnector(bdp.getLabel(), String.valueOf(bdp.getValue()));
    }

    /*Test of the class DatabaseConnector
    public static void main (String[] arg) {
        DatabaseConnector theDbc = DatabaseConnector.getInstance();
        while(true){
            theDbc.pushToDataBaseConnector("mesure", "8");
            Utility.waitSomeTime(1000);
        }
    }*/
}
