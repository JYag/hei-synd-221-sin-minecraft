package ch.hevs.isi;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.utils.Utility;

public class MinecraftController {
    public static boolean USE_MODBUS4J = false;

    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) {

        String dbHostName = "influx.sdi.hevs.ch";
        String dbName = "SIn16";
        String dbUserName = "SIn16";
        String dbPassword = "4dae48e9267d952ba12a0bb19303ca62";

        String modbusTcpHost = "localhost";
        int modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName = parameters[0];
            dbUserName = parameters[1];
            dbName = dbUserName;
            dbPassword = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }

        FieldConnector.config(modbusTcpHost,modbusTcpPort);
        DatabaseConnector.config(dbHostName,dbName, dbUserName, dbPassword);


        FieldConnector.getInstance().addFloatRegister("GRID_U_FLOAT",false,89,1000,0);
        FieldConnector.getInstance().addFloatRegister("BATT_P_FLOAT",false,57,6000,-3000);
        FieldConnector.getInstance().addFloatRegister("BATT_CHRG_FLOAT",false,49,1,0);
        FieldConnector.getInstance().addFloatRegister("SOLAR_P_FLOAT",false,61,1500,0);
        FieldConnector.getInstance().addFloatRegister("WIND_P_FLOAT",false,53,1000,0);
        FieldConnector.getInstance().addFloatRegister("COAL_P_FLOAT",false,81,600,0);
        FieldConnector.getInstance().addFloatRegister("COAL_AMOUNT",false,65,1,0);
        FieldConnector.getInstance().addFloatRegister("HOME_P_FLOAT",false,101,1000,0);
        FieldConnector.getInstance().addFloatRegister("PUBLIC_P_FLOAT",false,97,500,0);
        FieldConnector.getInstance().addFloatRegister("FACTORY_P_FLOAT",false,105,2000,0);
        FieldConnector.getInstance().addFloatRegister("BUNKER_P_FLOAT",false,93,500,0);
        FieldConnector.getInstance().addFloatRegister("WIND_FLOAT",false,301,1,0);
        FieldConnector.getInstance().addFloatRegister("WEATHER_FLOAT",false,305,1,0);
        FieldConnector.getInstance().addFloatRegister("WEATHER_FORECAST_FLOAT",false,309,1,0);
        FieldConnector.getInstance().addFloatRegister("WEATHER_COUNTDOWN_FLOAT",false,313,600,0);
        FieldConnector.getInstance().addFloatRegister("CLOCK_FLOAT",false,317,1,0);

        FieldConnector.getInstance().addFloatRegister("REMOTE_COAL_SP",true,209,1,0);
        FieldConnector.getInstance().addFloatRegister("REMOTE_FACTORY_SP",true,205,1,0);

        FieldConnector.getInstance().addBooleanRegister("REMOTE_SOLAR_SW",true,401);
        FieldConnector.getInstance().addBooleanRegister("REMOTE_WIND_SW",true,405);

        FieldConnector.getInstance().addFloatRegister("FACTORY_ENERGY",false,341,3600000,0);
        FieldConnector.getInstance().addFloatRegister("SCORE",false,345,3600000,0);

        FieldConnector.getInstance().addFloatRegister("COAL_ST",false,601,1,0);
        FieldConnector.getInstance().addFloatRegister("FACTORY_ST",false,605,1,0);
        FieldConnector.getInstance().addBooleanRegister("SOLAR_CONNECT_ST",false,609);
        FieldConnector.getInstance().addBooleanRegister("WIND_CONNECT_ST",false,613);

        SmartController sc = new SmartController();

        /*

        BinaryDataPoint bdp = new BinaryDataPoint("testBinary", true);
        FloatDataPoint fdp = new FloatDataPoint("testFloat", true);
        System.out.println("bdp default value = " + (bdp.getValue() ? "true":"false"));
        bdp.setValue(true);

        System.out.println("bdp " + bdp.getLabel() + " value = " + (bdp.getValue() ? "true":"false"));
        bdp.setValue(false);
        System.out.println("bdp " + bdp.getLabel() + " value = " + (bdp.getValue() ? "true":"false"));

        System.out.println("fdp default value = " + fdp.getValue());
        fdp.setValue(0.45f);
        System.out.println("fdp " + fdp.getLabel() + " value = " + fdp.getValue());
        fdp.setValue(0.75f);
        System.out.println("fdp " + fdp.getLabel() + " value = " + fdp.getValue());


        DataPoint dp = DataPoint.getDataPointFromLabel("testBinary");
        System.out.println("dp label = " + dp.getLabel());

        if (dp instanceof BinaryDataPoint) {
            System.out.println("dp " + dp.getLabel() + " value = " + ( ((BinaryDataPoint) dp).getValue() ? "true":"false"));
        }

        dp = DataPoint.getDataPointFromLabel("testFloat");
        System.out.println("dp label = " + dp.getLabel());

        if (dp instanceof FloatDataPoint) {
            System.out.println("dp " + dp.getLabel() + " value = " + ((FloatDataPoint) dp).getValue());


        }

        */

    }
}
