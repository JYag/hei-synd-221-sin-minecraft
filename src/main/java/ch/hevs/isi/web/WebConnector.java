package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.Vector;

/**
 * Connect and send data to the WebPage
 */
public class WebConnector extends WebSocketServer implements DataPointListener {

    private static WebConnector theWbc = null;

    /**
     * The static method getInstance() instance a WebConnector
     * if no WebConnector have already been created
     *
     * @return Instance of WebConnector
     */
    public static WebConnector getInstance() {
        if (theWbc == null) {
            theWbc = new WebConnector();
        }
        return theWbc;
    }

    Vector so = new Vector();

    /**
     * Create InetSocket with the port 8888
     */
    private WebConnector() {
        super(new InetSocketAddress(8888));
        start(); //for turn infinity
    }

    /**
     * Establishing the connection
     *
     * @param webSocket       the web socket
     * @param clientHandshake not used
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        so.add(webSocket);
        webSocket.send("Welcome to the World of Minecraft king");
    }

    /**
     * Close the connection
     *
     * @param webSocket the web socket
     * @param i         not used
     * @param s         not used
     * @param b         not used
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        webSocket.close();
        so.remove(webSocket);
    }

    @Override
    /** If something change on the web,the server receive a message. Look if it's a boolean or a float and after create a Datapoint.
     * @param webSocket the web socket
     * @param s String the word that changes
     */
    public void onMessage(WebSocket webSocket, String s) {
        String message[] = s.split("=");
        if (message.length >= 2) {
            String label = message[0];
            String value = message[1];
            if (value.equals("true") || value.equals("false")) {
                //System.out.println("the dpd " +label+ " is " +value);
                BinaryDataPoint bdp = (BinaryDataPoint) DataPoint.getDataPointFromLabel(label);
                bdp.setValue(Boolean.valueOf(value));
            } else {
                //System.out.println("the fpd " +label+ " is " +value);
                FloatDataPoint fpd = (FloatDataPoint) DataPoint.getDataPointFromLabel(label);
                fpd.setValue(Float.valueOf(value));
            }
        }
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
    }

    @Override
    public void onStart() {
        System.out.println("Start");
    }

    /**
     * Push a new value with the given label to the web page
     *
     * @param label the name of the DataPoint
     * @param value the value of the DataPoint
     */
    private void pushToWebPages(String label, String value) {
        for (WebSocket client : this.getConnections()) {
            if (client.isOpen()) {
                client.send(label + "=" + value);
            }
        }
    }

    /**
     * Push the new value of a float Datapoint to web page
     *
     * @param fdp Float data point
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        pushToWebPages(fdp.getLabel(), String.valueOf(fdp.getValue()));
    }

    /**
     * Push the new value of a binary Datapoint to web page
     *
     * @param bdp Binary data point
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        pushToWebPages(bdp.getLabel(), String.valueOf(bdp.getValue()));
    }


    public static void main(String[] arg) {

        WebConnector wc = new WebConnector().getInstance();
    }
}