package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

/**
 * Float DataPoint
 */

public class FloatDataPoint extends DataPoint {
    private float value;

    /**
     * Create a Float DataPoint.
     *
     * @param label    The label of the DataPoint.
     * @param isOutput Datapoint status (true = output, false = input)
     */
    public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Return the value of the DataPoint
     *
     * @return Value of DataPoint
     */
    public float getValue() {
        return value;
    }

    /**
     * Set the value of the DataPoint
     *
     * @param value Value of DataPoint
     */
    public void setValue(float value) {
        this.value = value;


        DatabaseConnector dbc = DatabaseConnector.getInstance();
        dbc.onNewValue(this);

        WebConnector wbc = WebConnector.getInstance();
        wbc.onNewValue(this);

        FieldConnector fdc = FieldConnector.getInstance();
        fdc.onNewValue(this);

    }
}
