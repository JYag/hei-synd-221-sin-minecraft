package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

/**
 * Binary DataPoint
 */
public class BinaryDataPoint extends DataPoint {
    private boolean value;

    /**
     * Create a Binary DataPoint.
     *
     * @param label    The label of the DataPoint.
     * @param isOutput Datapoint status (true = output, false = input)
     */
    public BinaryDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Get the value of the DataPoint
     *
     * @return Value of DataPoint
     */
    public boolean getValue() {
        return value;
    }

    /**
     * Set the value of the DataPoint
     *
     * @param value Value of the DataPoint
     */
    public void setValue(boolean value) {
        this.value = value;


        DatabaseConnector dbc = DatabaseConnector.getInstance();
        dbc.onNewValue(this);

        WebConnector wbc = WebConnector.getInstance();
        wbc.onNewValue(this);

        FieldConnector fdc = FieldConnector.getInstance();
        fdc.onNewValue(this);


    }
}
