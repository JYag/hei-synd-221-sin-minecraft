package ch.hevs.isi.core;

import com.sun.javafx.collections.MappingChange;

import java.util.HashMap;
import java.util.Map;

/**
 * Main DataPoint class
 */

public abstract class DataPoint {

    private String label;
    private Boolean isOutput;

    private final static Map<String, DataPoint> dataPointMap = new HashMap();

    /**
     * Create a DataPoint.
     *
     * @param label    The label of the DataPoint.
     * @param isOutput Datapoint status (true = output, false = input)
     */
    public DataPoint(String label, boolean isOutput) {
        this.label = label;
        this.isOutput = isOutput;
        dataPointMap.put(label, this);
    }

    /**
     * Return the DataPoint linked to a label
     *
     * @param label Label of the DataPoint
     * @return The Datapoint linked to the label
     */
    public static DataPoint getDataPointFromLabel(String label) {
        return dataPointMap.get(label);
    }

    /**
     * Return the label of the DataPoint
     *
     * @return The label of the DataPoint
     */
    public String getLabel() {
        return label;
    }

    /**
     * Return the status of the DataPoint (true = output, false = input)
     *
     * @return The status of the DataPoint
     */
    public boolean isOutput() {
        return isOutput;
    }


    /**
     * Return a float DataPoint linked to a label
     *
     * @param label Label of the DataPoint
     * @return The Datapoint linked to the label
     */
    public static DataPoint getFloatDataPointFromLabel(String label) {
        DataPoint dp = dataPointMap.get(label);
        if (dp instanceof FloatDataPoint) {
            return dp;
        } else {
            return null;
        }
    }

    /**
     * Return a binary DataPoint linked to a label
     *
     * @param label Label of the DataPoint
     * @return The Datapoint linked to the label
     */
    public static DataPoint getBinaryDataPointFromLabel(String label) {
        DataPoint dp = dataPointMap.get(label);
        if (dp instanceof BinaryDataPoint) {
            return dp;
        } else {
            return null;
        }
    }

}
