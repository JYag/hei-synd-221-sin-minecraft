package ch.hevs.isi.core;

/**
 * Interface that manage the new values for web and database connectors
 */
public interface DataPointListener {
    void onNewValue(FloatDataPoint fdp);
    void onNewValue(BinaryDataPoint bdp);
}
