package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Timer;
import java.util.TimerTask;

/** Connect to the Field
 */
public class FieldConnector implements DataPointListener {

    private static FieldConnector theFdc = null;

    /** Get instance of the FieldConnector
     * @return Instance of FieldConnector
     */
    public static FieldConnector getInstance() {
        if (theFdc ==  null){
            theFdc = new FieldConnector();
        }
        return theFdc;

    }
    /** Constructor of the class
     */
    private FieldConnector (){
        Timer timer = new Timer();
        long period = 2000;

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                BooleanRegister.poll();
                FloatRegister.poll();
            }
        }, 0,period);
    }


    /**
     * ModBus configuration
     *
     * @param host Ip address of the installation
     * @param port Port of the installation
     */
    public static void config(String host, int port){
        BooleanRegister.config(host,port);
        FloatRegister.config(host,port);
    }

    /**
     * @param address the Modbus address
     * @param label the name of the BinaryPoint object
     * @param isOutput is the register an input or output (true = output)
     */
    public void addBooleanRegister(String label, boolean isOutput, int address){
        BooleanRegister br = new BooleanRegister(isOutput, address, label);
        DataPoint bdp = BinaryDataPoint.getBinaryDataPointFromLabel(label);
    }
    /**
     * @param address the Modbus address
     * @param label the name of the FloatPoint object
     * @param isOutput is the register an input or output (true = output)
     * @param offset is the offset of the value
     * @param range is the range of the value
     */
    public void addFloatRegister(String label, boolean isOutput, int address, int range, int offset){
        FloatRegister fr = new FloatRegister(isOutput, address, label, offset, range);
        DataPoint fdp = FloatDataPoint.getFloatDataPointFromLabel(label);
    }

    /** New float value of DataPoint
     *  @param fdp float datapoint to write
     */

    @Override
    public void onNewValue(FloatDataPoint fdp) {
        FloatRegister.getRegisterFromDatapoint(fdp).write();

    }
    /** New binary value of DataPoint
     * @param bdp binary datapoint to write
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        BooleanRegister.getRegisterFromDatapoint(bdp).write();
    }
}
