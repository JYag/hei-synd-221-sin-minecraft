package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;

import java.util.ArrayList;
import java.util.HashMap;

public class BooleanRegister {

    private int address;

    private BinaryDataPoint datapoint;

    private static String modBusIP;
    private static int modBusHost;

    private static HashMap<BinaryDataPoint, BooleanRegister> dataPointMap = new HashMap<>();
    private static ArrayList<BooleanRegister> bRegList = new ArrayList<>();



    /**
     * Class constructor
     *
     * @param isOutput is the register an input or an output (true = output)
     * @param address  address of the register
     * @param label    label of the register
     */
    public BooleanRegister(boolean isOutput, int address, String label) {
        datapoint = new BinaryDataPoint(label, isOutput);
        dataPointMap.put(this.datapoint, this);
        this.address = address;

        bRegList.add(this);

    }


    /**
     * ModBus configuration
     *
     * @param host Ip address of the installation
     * @param port Port of the installation
     */
    public static void config(String host, int port){
        modBusIP = host;
        modBusHost = port;
    }

    /**
     * Read the content of the Minecraft world boolean variables
     */
    public void read() {
        datapoint.setValue(ModbusAccessor.getInstance(modBusIP, modBusHost).readBoolean(address));
    }

    /**
     * Write a boolean value in the Minecraft world variables
     */
    public void write() {
        ModbusAccessor.getInstance(modBusIP,modBusHost).writeBoolean(address, datapoint.getValue());
    }

    /**
     * Get the register from a binary Datapoint
     *
     * @param bdp BinaryDataPoint
     * @return register of the DataPoint
     */
    public static BooleanRegister getRegisterFromDatapoint(BinaryDataPoint bdp) {
        return dataPointMap.get(bdp);
    }

    /**
     * Periodic read of the registers
     */
    public static void poll() {
        for (BooleanRegister br : bRegList) {
            br.read();
        }
    }

}