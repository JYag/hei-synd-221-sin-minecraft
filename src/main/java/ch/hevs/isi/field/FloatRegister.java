package ch.hevs.isi.field;

import ch.hevs.isi.core.FloatDataPoint;

import java.util.ArrayList;
import java.util.HashMap;

public class FloatRegister {

    private static HashMap<FloatDataPoint, FloatRegister> dataPointMap = new HashMap<>();

    private FloatDataPoint datapoint;

    private static String modBusIP;
    private static int modBusHost;

    private int address;
    private int offset;
    private int range;

    private static ArrayList<FloatRegister> fRegList = new ArrayList<>();

    /**
     * Class constructor
     *
     * @param isOutput is the register an input or an output (true = output)
     * @param address  address of the register
     * @param label    label of the register
     * @param offset is the offset of the value
     * @param range is the range of the value
     */
    public FloatRegister(boolean isOutput, int address, String label, int offset, int range) {
        this.address = address;
        this.offset = offset;
        this.range = range;

        datapoint = new FloatDataPoint(label, isOutput);
        dataPointMap.put(this.datapoint, this);

        fRegList.add(this);

    }

    /**
     * ModBus configuration
     *
     * @param host Ip address of the installation
     * @param port Port of the installation
     */
    public static void config(String host, int port){
        modBusIP = host;
        modBusHost = port;
    }

    /**
     * Read the content of the Minecraft world float variables
     */
    public void read() {
        datapoint.setValue(ModbusAccessor.getInstance(modBusIP, modBusHost).readFloat(address) * range + offset);
    }

    /**
     * Write a float value in the Minecraft world variables
     */
    public void write() {
        ModbusAccessor.getInstance(modBusIP, modBusHost).writeFloat(address, (datapoint.getValue() - offset) / range);
    }

    /**
     * Get the register from a binary Datapoint
     *
     * @param fdp FloatDataPoint
     * @return register of the DataPoint
     */
    public static FloatRegister getRegisterFromDatapoint(FloatDataPoint fdp) {
        return dataPointMap.get(fdp);
    }

    /**
     * Periodic read of the registers
     */
    public static void poll() {
        for (FloatRegister fr : fRegList) {
            fr.read();
        }
    }
}