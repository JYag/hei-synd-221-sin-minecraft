package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

/**
 * Modbus Accessor
 */
public class ModbusAccessor {

    private static ModbusAccessor mBAcc = null;
    private ModbusMaster master;

    /**
     * Constructor of the class, init and connect to the specified host and port
     *
     * @param host Ip address of the Modbus
     * @param port Port of the Modbus
     */
    private ModbusAccessor(String host, int port) {

        IpParameters params = new IpParameters();
        params.setHost(host);
        params.setPort(port);

        master = new ModbusFactory().createTcpMaster(params, false);

        // Init and connect

        try {
            master.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
        }

    }

    /**
     * Create and return the ModbusAccessor (with ip : "localhost" and port : "1502")
     *
     * @return The Modbus Accessor
     */
    public static ModbusAccessor getInstance() {
        if (mBAcc == null) {
            mBAcc = new ModbusAccessor("localhost", 1502);
        }
        return mBAcc;
    }

    /**
     * Create and returns the ModbusAccessor (with parameters)
     *
     * @param host Ip address of the installation
     * @param port Port of the installation
     * @return ModbusAccessor
     */
    public static ModbusAccessor getInstance(String host, int port) {
        if (mBAcc == null) {
            mBAcc = new ModbusAccessor(host, port);
        }
        return mBAcc;
    }

    /**
     * @param RegAddress address of the modbus register
     * @return boolean value
     */
    public boolean readBoolean(int RegAddress) {
        try {
            return master.getValue(BaseLocator.coilStatus(1, RegAddress));
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param RegAddress address of the modbus register
     * @param newValue   boolean value to be written
     */
    public void writeBoolean(int RegAddress, boolean newValue) {
        try {
            master.setValue(BaseLocator.coilStatus(1, RegAddress), newValue);
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }

    }

    /**
     * @param RegAddress address of the modbus register
     * @return float value
     */
    public float readFloat(int RegAddress) {
        try {
            return master.getValue(BaseLocator.inputRegister(1, RegAddress, DataType.FOUR_BYTE_FLOAT)).floatValue();
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return 0;

    }

    /**
     * @param RegAddress address of the modbus register
     * @param newValue   float value to be written
     */
    public void writeFloat(int RegAddress, float newValue) {
        try {
            master.setValue(BaseLocator.holdingRegister(1, RegAddress, DataType.FOUR_BYTE_FLOAT), newValue);
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }

    }

    /*Test of the class
    public static void main(String[] args) {
        ModbusAccessor mBA = ModbusAccessor.getInstance();

        boolean run = true;
        while (run) {
            Boolean bValue = mBA.readBoolean(609);
            Float fValue = mBA.readFloat(605);

            mBA.writeBoolean(609, !bValue);
            mBA.writeFloat(605, 0.95f);

            if (fValue == null) {
                Utility.DEBUG("ModbusAccessor", "main", "Modbus Connection Error");
                run = false;
            } else {
                Utility.DEBUG("ModbusAccessor", "main", "Register 609 " + bValue);
                Utility.DEBUG("ModbusAccessor", "main", "Register 605 " + fValue);
                Utility.waitSomeTime(800);
            }

        }


    }*/


}
